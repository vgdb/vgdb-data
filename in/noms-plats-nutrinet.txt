acras cribiches (crevettes)
acras de morue
agneau korma
aiguillette de canard
aile de dinde rôtie
aile de poulet rôtie
aligot / truffade
anchoïade
anchois cuit
andouille
andouillette
aubergine farcie à la chair à saucisse
aubergine farcie à la chair à saucisse sans sel ajouté
avocat au crabe
avocat au crabe sans sel ajouté
avocat aux crevettes
avocat aux crevettes sans sel ajouté
avocat vinaigrette
avocat vinaigrette sans sel ajouté
baba au rhum
babelutte
bacon fumé
bagel
baklawa (pâtisserie orientale aux amandes et au sirop)
banh bao
barre chocolatée à la noix de coco (type Bounty)
barre chocolatée aux fruits oléagineux (type Snickers)
barre chocolatée biscuitée (type KitKat)
barre chocolatée biscuitée aux noisettes (type Kinder Bueno)
barre chocolatée fourrée aux fruits
barre chocolatée non biscuitée (type Mars)
barre goûter frais au cacao (type Kinder Pingoui)
barres glacées (type Mars glacé)
bâtonnet de dinde pané
bâtonnet de poisson pané
bâtonnet de surimi
bavarois aux fruits
beignet à la confiture
beignet de calamars
beignet de crabe
beignet de crevette
beignet de fruits
beignet de légumes
beignet de viande, volaille ou poisson
beignet nature (type merveilles, bugnes…)
biscuit apéritif allégé (type crackers)
biscuit apéritif fourré au fromage (type crakers)
biscuit apéritif nature (type crakers)
biscuit apéritif sans fromage
biscuit apéritif soufflé à base de maïs (type Curly)
biscuit apéritif soufflé à base de pomme de terre (type Monster Munch)
biscuit au chocolat (type Gerblé, Céréal...)
biscuit aux céréales (type Gerblé, Céréal...)
biscuit aux fruits (type Gerblé, Céréal...)
biscuit chocolaté
biscuit moelleux fourré (type Pim's)
biscuit nappé aux fruits, type barquette
biscuit pour petit déjeuner
biscuit sans gluten
biscuit sec
biscuit sec avec tablette de chocolat (type Petit Ecolier)
biscuit sec fourré aux fruits
biscuit sec nappage chocolat (type Pépito)
biscuit type langue de chat
biscuit type palmier
biscuit type petit beurre
biscuit, barre fourrée (supprimé)
blanc d'oeuf cru
blanc d'oeuf cuit
blanc de poulet rôti
blanc-manger coco
blanquette de veau
bleu
blinis
bo bun
boeuf à bourguignon ou carbonnade
boeuf à pot-au-feu
boeuf au curry
boeuf au curry sans sel ajouté
boeuf bourguignon
boeuf braisé
boeuf carottes
boisson lactée avec jus de fruits
boisson maltée cacaotée (type Tonimalt, Ovomaltine)
bokit (pâte)
bonbon
bonbon au caramel
bonbon caramel et chocolat (type Michoko, Roll)
bonbon gélifié (type crocodile)
bonbon praline
bonbon sans sucre
bortch
bouchée à la reine au poisson et fruits de mer
bouchée à la reine au poulet
boudin antillais
boudin blanc
boudin noir
boudoir / biscuit à la cuillère
bouillabaisse
bouillon de légumes deshydraté reconstitué
boulettes à la liégeoise, sauce lapin
boulettes d'agneau (type kefta)
boulettes de bœuf
boulettes de veau
brandade de morue (sans pommes de terre)
brandade de morue parmentière
bretzel alsacien salé
bretzel apéritif salé
bretzel en pâtisserie
brioche
brioche aux pépites de chocolat
brioche individuelle
brochette d'agneau/mouton
brochette de bœuf
brochette de crevettes
brochette de poisson
brochette de porc
brochette de viandes mixtes
brochette de volailles
brousse de brebis
brownie au chocolat
brownie au chocolat et aux noix
bruschetta, tartine au jambon
bûche de Noël
burrito au boeuf
burrito viande et haricots
cacahuètes enrobées de chocolat (type M&M's)
cacahuètes grillées non salées
cacahuètes grillées salées
café liégeois glacé
café viennois
cake à la frangipane
cake au fromage
cake au poisson
cake aux fruits
cake aux légumes
cake aux raisins
cake aux raisins secs sans sel ajouté
cake salé jambon, olives
calamar à l'américaine
calamar farci
calamar frit
calisson
calmar
cambozola
camembert 40% MG (19% sur produit fini)
camembert 45% MG (22% sur produit fini)
camembert 50% MG (25% sur produit fini)
camembert 60% MG (31% sur produit fini)
canapés (garnitures diverses)
canard à l'orange
canard laqué
canard rôti
cancoillotte
canelé
cannelloni à la viande
cappuccino
cappuccino en poudre
caramel
caramel au beurre
carbonnade flammande
cari de porc
cari de poulet
cari de saucisse
carpaccio de boeuf
carpaccio de saumon à l'huile d'olive
carré aux dattes et aux noix
carré de l'est
carré frais (supprimé)
carrelet
cassolette de noix de Saint-Jacques aux poireaux
cassolette de poisson ou fruits de mer
cassoulet
caviar d'aubergine
céleri rémoulade
céréales chocolatées non fourrées (type Chocapic)
céréales fourrées au chocolat ou chocolat-noisettes
céréales fourrées sans chocolat
céréales pour petit déjeuner sans gluten
céréales riches en fibres
cervelas
cervelle d'agneau / mouton
cervelle d'agneau / mouton sautée
cervelle de porc braisée
cervelle de veau
cervelle de veau sautée
chabichou
chair à saucisse
charlotte au chocolat
charlotte aux fruits
chaudeau
chausson aux pommes
chawarma de poulet
cheddar
cheeseburger
chili con carne
chili con carne sans sel ajouté
chipolata (saucisse de porc) nature ou aux herbes
chocolat au lait
chocolat au lait avec fruits secs et noisettes
chocolat au lait avec riz soufflé
chocolat au lait chaud
chocolat au lait et aux noisettes (type Ferrero rocher)
chocolat au lait fourré (praline)
chocolat blanc
chocolat en poudre sucré (type Nesquik)
chocolat en poudre sucré, sans gluten
chocolat liégeois glacé
chocolat noir fourré (praline)
chorba
chorizo sec
chou à la crème pâtissière
chou farci à la chair à saucisse
chou farci à la chair à saucisse sans sel ajouté
chou romanesco cuit
choucroute
choucroute de la mer
choucroute garnie
chouquette
cigarette russe
clafoutis
colombo de mouton ou de cabri
colombo de porc
colombo de poulet
colombo en poudre
cône chocolat pistache
cône vanille
cône, autre parfum
confiserie au chocolat dragéifiée (type Smarties)
confit de canard / oie
confiture de lait
consommé de boeuf
cookie
coppa
coq au vin
coq au vin sans sel ajouté
coquelet
coques
coquille de noël, cougnou
coquille de poisson
coquille saint jacques, noix
coquille Saint-Jacques préparée
coquille Saint-Jacques, noix et corail
cordon bleu
corne de gazelle
corned beef
côte d'agneau / mouton
côte de boeuf grillée
côte de porc grillée
côte de veau
côte de veau braisée
côte de veau maigre braisée
côte de veau maigre rôtie
côte de veau rôtie
côtelette d'agneau / mouton grillée
coulommiers
couque à la crème
couque aux raisins
court-bouillon de poisson
couscous au mouton
couscous au poisson
couscous au poisson sans sel ajouté
couscous au poulet
couscous royal
craquelin / cramique
crème à la vanille allégée en matière grasse
crème anglaise
crème au beurre
crème au chocolat allégée en matière grasse
crème aux œufs
crème brûlée
crème caramel
crème chantilly
crème chantilly allégée
crème de whisky (type Baileys)
crème de yaourt aromatisé
crème de yaourt aux pépites de chocolat
crème de yaourt nature
crème dessert
crème dessert à la vanille
crème dessert au café
crème dessert au chocolat
crème dessert café liégeois
crème dessert chocolat liégeois
crème fraîche épaisse
crème fraîche épaisse allégée
crème fraîche épaisse ultra-légère
crème fraîche liquide
crème fraîche liquide allégée
crème fraîche liquide ultra-légère
crème glacée à la fraise
crème glacée à la vanille
crème glacée à la vanille allégée en sucre et matière grasse
crème glacée au chocolat
crème glacée, autre parfum
crème pâtissière
crêpe au jambon
crêpe au sarrasin (galette)
crêpe aux champignons
crêpe fourrée à la confiture du commerce
crêpe fourrée au chocolat du commerce
crêpe fourrée béchamel, fromage
crêpe fourrée béchamel, jambon
crêpe fourrée béchamel, jambon, fromage
crêpe fourrée béchamel, jambon, fromage, champignons
crêpe nature
crêpe sucrée
crêpe sucrée sans sel ajouté (supprimé)
crevette grise
crevette rose
croissant au beurre
croissant au jambon
croissant aux amandes
croissant ordinaire
croquant aux amandes
croque hawaien à l'ananas
croque provençal à la tomate
croque-madame à l'œuf
croque-monsieur
croque-monsieur avec béchamel
croquette de poisson
croquette de pomme de terre
croquettes de pois chiches (falafel)
crumble aux fruits rouges
crumble aux pommes
crumpet
cuberdon
cuisse de dinde rôtie avec peau
cuisse de dinde rôtie sans peau
cuisse de grenouille
cuisse de poulet laqué
cuisse de poulet rôtie (haut de cuisse)
cuisse de poulet rôtie (pilon)
cuisse de poulet rôtie avec peau
cuisse de poulet rôtie sans peau
curry /  massala en poudre
curry d'agneau / mouton
curry de poisson
dal (dahl) de lentilles
daube de poisson
dombré aux crevettes
dombré aux haricots rouges
donut à la vanille
donut au chocolat
donut nature
double cheeseburger
double hamburger
dragée
échine de porc rôti
éclair chocolat, café
écrevisse
emmental
emmental allegé
enchilada au bœuf et fromage
enchilada au fromage
entrecôte de boeuf grillée
épaule d'agneau / mouton maigre braisée
épaule d'agneau / mouton maigre rôtie
épaule d'agneau / mouton rôtie
épaule de veau
épaule de veau braisée
escalope de dinde rôtie
escalope de poulet
escalope de poulet panée
escalope de veau
escalope de veau à la milanaise (panée)
escargot
esquimau ou bâtonnet vanille
esquimau ou bâtonnet, autre parfum
faisselle 0% MG
faisselle 40% MG nature (6% sur produit fini)
fajitas à la viande
far breton
faux-filet de boeuf grillé
féroce d'avocat
féta 45% MG
feuilleté au fromage
feuilleté au poisson
feuilleté au saumon
feuilleté aux abricots
feuilleté aux amandes et/ou cacahuètes
feuilleté aux fruits
feuilleté aux fruits de mer
feuilleté aux légumes
feuilleté fourré béchamel, jambon, fromage
filet d'anchois à l'huile
filet d'anchois à l'huile d'olive
filet d'anchois à l'huile d'olive, égoutté
filet d'anchois à l'huile, égoutté
filet d'Anvers
filet de bacon
filet de hoki
filet de maquereau à l'huile, égoutté
filet de maquereau au vin blanc
filet de maquereau sauce moutarde
filet de maquereau sauce tomate
filet de poisson pané
filet de porc maigre
filet de veau rôti
financier
fines tranches d'emmental (type Leerdammer)
fines tranches d'emmental allégées (type Leerdammer)
flamiche picarde
flammenkueche
flan
flan aux fruits
flan aux légumes
flan caramel
flan coco
flan de courgettes
flan pâtissier / tarte au flan
flanchet / bavette de boeuf
fond de sauce
fondue bourguignone
fondue de poireaux
fondue savoyarde
forêt noire
fougasse aux lardons
fougasse aux olives
fougasse garnie (supprimé)
fougasse nature
fourme d'Ambert
fraise melba
fraisier
framboisier
friand / feuilleté à la saucisse
friand / feuilleté à la viande
friand / feuilleté au fromage
fricadelle (fricandelle)
fromage à pâte ferme, type Bonbel - Babybel
fromage à raclette
fromage ail et fines herbes (type Boursin, Tartare)
fromage ail et fines herbes allégé (type Tartare)
fromage apéritif en cube (type Apéricube)
fromage blanc 0% MG aromatisé
fromage blanc 0% MG aux fruits, édulcoré
fromage blanc 0% MG nature
fromage blanc 20% MG aux fruits (6% sur produit fini)
fromage blanc 20% MG nature (3% sur produit fini)
fromage blanc 40% MG aux fruits (6% sur produit fini)
fromage blanc 40% MG nature (6% sur produit fini)
fromage blanc et crème fouettée aux fruits (type Gervita aux fruits)
fromage blanc et crème fouettée nature (type Gervita nature)
fromage cottage nature
fromage d'abbaye (type Chimay, Maredsous, Passendale …)
fromage de brebis (pâte molle)
fromage de brebis à pâte dure (type Ossau-Iraty)
fromage de chèvre à tartiner (type Chavroux)
fromage de chèvre frais
fromage de chèvre mi-sec
fromage de chèvre sec
fromage de chèvre type bûche
fromage de tête
fromage fondu 25% MG
fromage fondu 45% MG
fromage fondu 65% MG
fromage fondu 70% MG
fromage fondu aux noix (type Rambol)
fromage frais demi-sel nature (type Carré frais)
fromage frais nature (type Carré frais)
fromage frais nature à 0% de MG (type carré frais)
fromage pâte ferme 25% MG
fromage pâte molle 25% MG
fromage type camembert 25% MG
galantine
galette bretonne (type st Michel)
galette de céréales soufflées (quinoa, avoine, seigle…)
galette de maïs, tortilla (sans garniture)
galette de pomme de terre (rösti)
galette de semoule (type kesra)
galette des rois à la frangipane
galette ou palet de légumes
gambas
gaspacho
gâteau à l'ananas
gâteau à la crème au beurre, type Javanais
gâteau à la farine de riz
gâteau au chocolat
gâteau au chocolat sans sel ajouté (supprimé)
gâteau au fromage (type cheesecake)
gâteau au yaourt
gâteau aux amandes
gâteau aux cheveux d'ange
gâteau aux noix
gâteau aux pommes
gâteau basque
gâteau breton
gâteau coco
gâteau de pain (type pain perdu)
gâteau de riz
gâteau de Savoie
gâteau de Savoie sans sel ajouté (supprimé)
gâteau de semoule
gâteau marbré au chocolat
gâteau moelleux aux fruits type mini-roulé ou mini cake fourré
gâteau moelleux fourré chocolat, pépites de chocolat ou lait
gâteau moelleux nature, type mini-cake
gaufre
gaufre de Bruxelles
gaufre de Liège
gaufre de Liège au chocolat
gaufrette
gaufrette au chocolat
gaufrette aux fruits
gélatine
gigot d'agneau / mouton rôti
gombo
gorgonzola
gouda
gougère
goulash
goulash sans sel ajouté
goûter fourré autre parfum (type BN)
goûter fourré chocolat ou fruit (type BN)
gratin d'endives (chicons) et jambon
gratin dauphinois
gratin de blettes
gratin de christophine
gratin de fruits
gratin de fruits de mer
gratin de légumes
gratin de pâtes
gratin de poisson
gratin de pomme de terre
gressins
grog
gruyère
gruyère rapé
guimauve
hachis mélangé (préparation  belge)
hachis parmentier
hamburger à la viande
hamburger au bacon
hamburger au poisson
hamburger au poulet pané
harira
herve
homard
hot dog (1 saucisse)
hot dog (2 saucisses)
houmous
île flottante, œufs en neige
irish coffee
jambon végétal
jaune d'oeuf cru
jaune d'oeuf cuit
kéfir aux fruits
kéfir de lait
Kiri
kouglof
kouign-aman
koulibiac de poisson
lacquemant
langouste
langoustine
langres
lapin
lapin à la moutarde
lapin aux pruneaux
lapin chasseur
lard gras
lardons fumés, crus
lardons fumés, cuits
lardons natures, crus (supprimé)
lardons natures, cuits
lasagnes
lasagnes au poisson
lasagnes aux épinards
lasagnes aux légumes
lassi aux fruits
légumes farcis à la viande ou à la volaille (sauf tomate)
lentilles
lentilles cuisinées
liégeois de fruits
longe et palette de porc cuites
loukoum
macaron
macédoine de légumes à la mayonnaise
madeleine
mafé
magret de canard fumé
magret de canard rôti
magret de canard séché
mai tai
maki
maki californien
maqrout (makrout)
mascarpone
massepain
mayonnaise
merguez
meringue
merveilleux
mesclun
milk-shake
mille-feuilles
mimolette
minestrone
mini boudin
mini-barres glacées (type mini Mars glacé)
moka au chocolat
mont blanc
morbier
mortadelle
morue
moule
moussaka
moussaka sans sel ajouté
mousse au chocolat
mousse aux fruits
mousse de marron (type Maronsui's)
mousse, autre parfum
mozzarella
muffin anglais complet
muffin anglais nature
muffin au chocolat
muffin aux fruits
mystère glacé
nachos au fromage
nachos au fromage et piment jalapeno
nan au fromage
nappage caramel
navarin d'agneau/mouton
nems aux légumes
nougat
nougat chinois
nougat glacé
nougat pistache
nougatine
nouilles chinoises
nouilles ou vermicelles de riz
nouilles sautées chinoises
nouilles sautées chinoises au bœuf
nouilles sautées chinoises au poulet
nouilles sautées chinoises aux crevettes
nuggets de volaille
oeuf brouillé
oeuf frit
oeuf poché
oeufs de poisson
omelette au fromage
omelette au jambon
omelette aux champignons
omelette aux fines herbes
omelette aux lardons
omelette aux pommes de terre
omelette nature
omelette norvégienne
osso bucco
paëlla
paëlla sans sel ajouté
pain viennois ou brioché
palet aux amandes
palette de veau braisée
palette de veau maigre braisée
palette de veau maigre rôtie
palette de veau rôtie
pan bagnat
panacotta
pancakes
pancetta, poitrine de porc roulée sèche
panettone
panini aux fromages
panini tomate mozarella jambon
papillote de poisson aux légumes
paris brest
parmesan en poudre
pâté à base de poisson ou de crustacés
pâte à chou, cuite
pâte à pizza
pâte à pizza sans gluten
pâte à tartiner au chocolat et aux noisettes (type nutella)
pâte à tartiner au chocolat sans noisettes (type chocopasta)
pâte à tartiner aux speculoos
pâte brisée
pâte d'amande
pâté de campagne
pâté de crevette ou homard
pâté de foie de porc
pâté de foie de volaille
pâte de fruits
pâté de lapin
pâte de sésame (tahin)
pâté en croûte
pâte feuilletée
pâté impérial/nem
pâte sablée
pâté/terrine
pâtes à la bolognaise
pâtes à la carbonara
pâtes au saumon
pâtes aux 3 fromages
pâtes aux fruits de mer
pâtés salés (porc)
pâtes sans gluten cuites
pâtes sauce tomate
paupiette de poisson
paupiette de porc
paupiette de veau
paupiette de volaille
pecorino
pepperoni
perle de coco (pâtisserie asiatique)
petit salé aux lentilles
petit suisse 0% MG
petit suisse 20% MG
petit suisse 40% MG (9% sur produit fini)
petit suisse 60% MG (19% sur produit fini)
petit suisse aromatisé
petit suisse au chocolat
petit suisse aux fruits
petits fours sucrés
pilon et cuisse de poulet panés
pintade
piperade
pissaladière
pizza
pizza 4 fromages
pizza 4 saisons
pizza à la viande hâchée
pizza au fromage, viande et légumes
pizza avec pepperoni
pizza chèvre lardons
pizza fromage (supprimé)
pizza jambon, fromage
pizza margherita
pizza poisson, fruits de mer
pizza royale (jambon, fromage, champignons)
pizza savoyarde
pizza type calzone
poêlée forestière
poêlée méridionale
poêlée montagnarde / savoyarde
poêlée paysanne / campagnarde
poire belle Hélène
poivron farci à la chair à saucisse
poivron farci au riz
pomme au four
pomme cajou
pomme cannelle
pomme dauphine salée
pomme de terre à l'huile
pomme de terre à l'huile sans sel ajouté
pomme de terre au four
pomme de terre cuite à l'eau
pomme de terre cuite à la vapeur
pomme de terre sautée
pomme duchesse
pomme malaka (pomme d'eau)
pomme noisette salée
porc au caramel
pot-au-feu
potage de brocoli
potée auvergnate
potée liégeoise
poulet au curry
poulet au curry avec riz ou semoule
poulet au curry sans sel ajouté
poulet basquaise
poulet basquaise et pâtes
poulet basquaise et riz
poulet cocotte ou grand-mère
poulet en préparation chinoise
poulet tikka / massala
poulet yassa
pralin
préparation à la viande à farcir
profiterole
pudding aux pruneaux
pudding ou bodding
purée d'amande
purée d'avocat (guacamole)
purée d'avocat (guacamole) sans sel ajouté
purée de carottes
purée de haricots verts
purée de légumes
purée de légumes sans sel ajouté
purée de marron
purée de pois cassés
purée de pomme de terre industrielle (type Mousline)
purée de pommes de terre
quatre-quarts (cake au beurre)
quatre-quarts (cake au beurre) sans sel ajouté
quenelle au naturel en sauce
quenelle nature
quiche au boeuf
quiche au fromage
quiche au poisson
quiche au poulet
quiche aux légumes
quiche aux poireaux
quiche lorraine
ragoût de cochon / porc
ragoût de lapin
ragoût de lièvre
ragoût de poule
raviole
ravioli au tofu
ravioli aux légumes
ricotta
rillettes
rillettes de canard
rillettes de maquereaux
rillettes de saumon
rillettes de thon
risotto à la milanaise
risotto au poisson ou fruits de mer
risotto au poulet
risotto aux champignons
risotto aux légumes
riz au lait
riz au lait sans sel ajouté
rocher coco
rognons d'agneau / mouton
rosbif de boeuf
rôti de porc
rôti de porc, filet maigre
rôti de veau
rôti de veau orloff
rougail de saucisses
roulé au fromage
rouleau de printemps
rouleau de printemps sans sel ajouté
sablé
sablé sans sel ajouté
salade caesar (césar)
salade coleslaw
salade crétoise / grecque
salade de chèvre chaud
salade de fruits frais non sucrée
salade de légumes
salade de légumes avec crevettes
salade de légumes avec poulet
salade de légumes avec thon
salade de pâte végétarienne
salade de pâtes aux légumes avec poulet
salade de pâtes aux légumes avec thon ou viande
salade de poisson mayonnaise
salade de pomme de terre assaisonnée
salade de pomme de terre nature
salade de poulet au curry mayonnaise
salade de riz
salade de taco
salade de taco avec chili con carne
salade endive (chicon), noix et roquefort
salade Landaise
salade liégeoise
salade macédoine et thon
salade niçoise
salade piémontaise
salade thaï
salade verte
salade verte mélangée
samos
samossa (samoussa) à la volaille
samossa (samoussa) au bœuf
samossa (samoussa) au porc
samossa (samoussa) au thon
sandwich camembert
sandwich camembert beurre
sandwich camembert emmental
sandwich crudités
sandwich crudités dinde
sandwich crudités dinde mayonnaise
sandwich crudités emmental
sandwich crudités emmental beurre
sandwich crudités jambon
sandwich crudités jambon beurre
sandwich crudités jambon œuf dur beurre
sandwich crudités mayonnaise
sandwich crudités œuf dur
sandwich crudités œuf mayonnaise
sandwich crudités porc mayonnaise
sandwich crudités poulet
sandwich crudités poulet mayonnaise
sandwich crudités rosbif
sandwich crudités rôti de porc
sandwich crudités surimi
sandwich crudités surimi mayonnaise
sandwich crudités thon
sandwich crudités thon mayonnaise
sandwich jambon
sandwich jambon beurre
sandwich jambon cornichon beurre
sandwich jambon de pays beurre
sandwich jambon gruyére
sandwich jambon gruyère beurre
sandwich merguez (1 merguez)
sandwich merguez (2 merguez)
sandwich merguez frites
sandwich pâté
sandwich pâté beurre
sandwich rillette
sandwich rillette beurre
sandwich rosette
sandwich rosette beurre
sandwich salami
sandwich salami beurre
sandwich saucisson à l'ail
sandwich saucisson à l'ail beurre
sandwich saucisson sec
sandwich saucisson sec beurre
sandwich saumon fumé
sandwich saumon fumé beurre
sandwich saumon fumé mayonnaise
sandwich viande
sauce à la crème
sauce aigre-douce
sauce aïoli
sauce américaine
sauce armoricaine
sauce au curry
sauce au fromage
sauce au lait de coco (type "thaï")
sauce au roquefort
sauce au vin blanc
sauce au vin rouge
sauce au yaourt
sauce barbecue
sauce basquaise
sauce béarnaise
sauce béarnaise allégée
sauce béchamel
sauce béchamel allégée en MG
sauce beurre blanc
sauce blanche
sauce bolognaise
sauce bordelaise
sauce bourguignonne
sauce caesar
sauce carbonara
sauce champignons (archiduc)
sauce chasseur
sauce chien
sauce chili
sauce cocktail
sauce court-bouillon créole
sauce de soja
sauce dressing au yaourt
sauce escabèche
sauce hollandaise
sauce madère
sauce marchand de vin
sauce mornay
sauce moutarde
sauce nantua
sauce napolitaine
sauce nuoc-mam
sauce pesto
sauce piccalilly
sauce piquante
sauce piquante antillaise
sauce poivre vert
sauce provençale
sauce salade moutarde
sauce tandoori
sauce tartare
sauce tomate
sauce tomate sans sel ajouté
sauce worcestershire
sauce yakitori
saucisse alsacienne fumée (gendarme)
saucisse cocktail
saucisse de Francfort
saucisse de Montbéliard
saucisse de Morteau
saucisse de Strasbourg (type Knacki)
saucisse de Toulouse
saucisse de volaille
saucisse sèche
saucisse végétarienne au tofu (type Tofinelle)
saucisson à l'ail
saucisson de jambon (type Saint Agaûne)
saucisson sec
saumon
saumon accompagné de courgettes
saumon accompagné de riz
saumon cru / sashimi
saumon fumé
saumon sauvage
sauté de veau
scone sucré
semoule (graine de couscous)
semoule au lait
semoule de mais (polenta)
smoothie aux fruits
smoothie lacté aux fruits
soufflé au fromage
soupe à l'oignon
soupe à l'oseille
soupe au pistou
soupe au potiron (supprimé)
soupe aux asperges
soupe aux champigons
soupe aux choux
soupe aux courgettes
soupe chinoise
soupe de carottes
soupe de chou-fleur
soupe de concombre
soupe de cresson
soupe de fèves
soupe de fèves et bacon
soupe de légumes deshydratée, reconstituée
soupe de légumes variés
soupe de lentilles
soupe de poireaux
soupe de poireaux et pommes de terre
soupe de pois cassés
soupe de poisson
soupe de potiron
soupe de poulet et légumes
soupe de tomate
soupe de tomate et légumes
soupe de vermicelles
soupe miso
soupe poulet vermicelle
soupe thaï
soupe type poule au pot
soupe z'habitant
speculoos
steak de soja au curry
steak de soja aux fines herbes
steak de soja aux petits légumes
steak de soja nature
steak de soja tomate-basilic
steak de soja tomate-poivron
steak haché de boeuf 10% MG cru
steak haché de boeuf 10% MG cuit
steak haché de boeuf 15% MG cru
steak haché de boeuf 15% MG cuit
steak haché de boeuf 20% MG cru
steak haché de boeuf 20% MG cuit
steak haché de boeuf 5% MG cru
steak haché de boeuf 5% MG cuit
steak haché de veau grillé
steak tartare (bœuf)/ filet américain
steak/bifteck de boeuf grillé
strudel aux pommes
substitut de viande mycoprotéiné (type Quorn)
substitut de viande mycoprotéiné pané (type Quorn)
surimi de crabe
surimi de crevette
surimi de pétoncle
sushi au saumon
sushi au thon
taco/burrito garni
tajine à la viande
tajine à la viande sans sel ajouté
tajine au poisson
tajine au poisson sans sel ajouté
tajine de légumes
tajine de poulet
tajine poulet et semoule
tapenade
tapioca
tarama
tarte à l'oignon
tarte à la crème
tarte à la morue
tarte à la rhubarbe
tarte à la tomate
tarte al d'jote
tarte au chocolat
tarte au citron
tarte au citron meringuée
tarte au coco
tarte au fromage
tarte au riz
tarte au saumon
tarte au sucre
tarte au thon
tarte aux amandes, amandine
tarte aux épinards
tarte aux fraises
tarte aux fruits
tarte aux fruits de mer
tarte aux légumes
tarte aux noix
tarte aux poireaux
tarte aux poires et chocolat
tarte aux pommes
tarte chèvre et tomate
tarte tatin aux pommes
tartelette aux pommes
tartiflette
tchep djen
tchep djen avec riz
tempeh
terrine de canard
terrine de poisson
terrine ou mousse de légumes
tian à la provençale
tiramisu
tiramisu aux fruits
tofu
tomate
tortellini à la viande
tourte aux légumes
tourte aux myrtilles
tourte aux pommes de terre
tourteau fromager
tranche de blanc de poulet doré au four
tranche de filet de dinde braisé
travers de porc braisé
trianon
tropézienne
truffe au chocolat
tzatziki (fromage blanc au concombre)
vacherin aux fruits
vacherin, mont d'or
veau marengo
velouté d'asperges
velouté de légumes verts
velouté de tomate
velouté de topinambours
vermicelle
vermicelles de soja
viande en sauce (supprimé)
vinaigrette
vinaigrette au yaourt
waterzoï de poulet
wrap au poisson
wrap au poulet
wrap aux légumes
yaourt  au lait entier aux céréales
yaourt 0% MG aromatisé, sucré
yaourt 0% MG au bifidus nature
yaourt 0% MG aux céréales
yaourt 0% MG aux fruits, édulcoré
yaourt 0% MG aux fruits, sucré
yaourt 0% MG brassé
yaourt 0% MG ordinaire
yaourt à boire à la pulpe de fruit
yaourt à boire aromatisé
yaourt à boire sans sucre
yaourt à la grecque
yaourt aromatisé
yaourt au bifidus aromatisé
yaourt au bifidus aux fruits
yaourt au bifidus nature
yaourt au lait de brebis
yaourt au lait de chèvre nature
yaourt au lait entier aromatisé
yaourt au lait entier aux fruits
yaourt au lait entier nature
yaourt au lait entier nature sucré
yaourt au soja aux fruits
yaourt au soja nature
yaourt aux fruits
yaourt aux fruits enrichi en stérols végétaux (type danacol)
yaourt brassé aux fruits
yaourt brassé nature
yaourt nature
yaourt nature enrichi en stérols végétaux (type danacol)
yaourt nature sucré
zlabia
