#!/bin/bash

HOST=$1
USER=${1:-} && shift
PASS=${1:-} && shift

[[ -z "$HOST" ]] && echo >&2 no hostname provided && exit 1
DIR="$(dirname "$(realpath $0)")"
if [[ $PWD != $DIR ]]; then
    [[ ! -d $DIR/out ]] && echo >&2 no out/ directory inside $DIR && exit 1
    cd $DIR
fi

git pull
# ~/.netrc possible
wget --user=$USER "http://$HOST/vgdb/export.php?out=dl" -O out/export.csv
wget --user=$USER "http://$HOST/vgdb/export.php" -O out/export.html
wget --password=$PASS "http://$HOST/vgdb/outils.php?dump=1" -O out/dump.json
wget --password=$PASS "http://$HOST/vgdb/outils.php?dump-masvol=1" -O out/dump-masvol.json

git add out/
git commit -m "autoupdate: $(date +"%F %T")"
echo "please go to $DIR and type: git push if you're satisfied with changes"
